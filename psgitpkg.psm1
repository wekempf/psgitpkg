$ModuleDir = $PSScriptRoot

$git = $null
foreach ($path in $env:Path.Split(';')) {
    $gitpath = Join-Path $path "git.exe"
    if (Test-Path $gitpath) {
        $git = $gitpath
        break
    }
}

if (-not $git) {
    throw "psgitpkg requires Git to be installed and in the PATH."
}

$GitPSModules = @{
    'psgitpkg' = 'https://wekempf@bitbucket.org/wekempf/psgitpkg.git'
    'psprompt' = 'https://wekempf@bitbucket.org/wekempf/psprompt.git'
    'posh-git' = 'https://github.com/dahlbyk/posh-git.git'
}
Export-ModuleMember -Variable $GitPSModules

<#
.Synopsis
   Installs a PowerShell module from a Git repository.
.DESCRIPTION
   If the specified module isn't installed it will be installed
   from the Git module.
.EXAMPLE
   Install-GitPSModule ssh://git@bitbucket.org/wekempf/psgitpkg.git
.EXAMPLE
   Install-GitPSModule ssh://git@bitbucket.org/wekempf/psgitpkg.git -Update -Import
#>
function Install-GitPSModule
{
    [CmdletBinding()]
    [OutputType([void])]
    Param
    (
        # The Git repository that contains the module to be installed.
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $Repository,

        # If the module is already installed then ensure it is up to date.
        [switch]
        $Update,

        # After installing the module import it into the current PowerShell session.
        [switch]
        $Import,

        # Imports the module only into the specified scope.
        $Scope = 'local'
    )

    Begin {
        trap {
            break
        }

        if ($GitPSModules.Contains($Repository)) {
            $module = $Repository
            $Repository = $GitPSModules[$module]
        } else {
            $module = Split-Path -Leaf $Repository
            $module = [System.IO.Path]::GetFileNameWithoutExtension($module)
        }

        $modules = Join-Path (Split-Path $Profile.CurrentUserAllHosts) Modules
        if (-not (Test-Path $modules)) {
            md $modules | Out-Null
        }

        Write-Verbose "Repository: $Repository"
        Write-Verbose "Module: $module"
        Write-Verbose "Installing to '$modules'..."
    }

    Process {
        pushd $modules
        try {
            if (Test-Path $module) {
                Write-Verbose 'Module is already installed.'
                cd $module
                if (Test-Path '.git') {
                    if ($Update) {
                        Write-Verbose 'Updating module...'
                        git pull | Out-Null
                    }
                } else {
                    Write-Warning "Module '$module' already exists and doesn't appear to be a git clone."
                }
            } else {
                Write-Verbose 'Cloning repository...'
                git clone $Repository | Out-Null
            }
            if ($Import) {
                Write-Verbose 'Importing module...'
                Import-Module $module -Force -Scope:$Scope
            }
        } finally {
            popd
        }
    }

    End {
    }
}
Export-ModuleMember -Function Install-GitPSModule